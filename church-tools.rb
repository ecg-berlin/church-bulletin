require 'faraday'
require 'faraday/net_http'
require 'faraday_middleware'
require 'faraday-cookie_jar'
require 'tzinfo'

Faraday.default_adapter = :net_http


def ct_init()
    # see https://ecghellersdorf.church.tools/api
    $ct = Faraday.new(url: $config['churchtools']['url'], headers: { 'Content-Type' => 'application/json'}) do |faraday|
        faraday.use Faraday::Response::RaiseError
        faraday.use :cookie_jar
        faraday.request :json # encode req bodies as JSON and automatically set the Content-Type header
        faraday.response :json # decode response bodies as JSON
        faraday.adapter :net_http
    end
    
    $ct.get('?q=login/ajax&func=loginWithToken&token='+$config['churchtools']['login_token'])
end

def birthdays(start_date, end_date)
    response = $ct.get('api/persons/birthdays', {start_date: start_date.to_s, end_date: end_date.to_s}) 
    response.body["data"]
end

def events(start_date, end_date)
    response = $ct.get('api/events', {from: start_date.to_s, to: (end_date+1).to_s}) 
    response.body["data"]
end

def termine(category_ids, start_date, end_date)
    now = Date.today
    from = ((start_date-1)-now).to_i
    to = ((end_date+1)-now).to_i
    response = $ct.get('?q=churchcal/ajax&func=getCalendarEvents', {:from => from, :to => to, 'category_ids' => category_ids}) 
    response.body['data'].sort_by { |t| t['startdate'] }
end

def history(start_date, end_date)
    ret = []
    page = 1
    while true
        response = $ct.get('api/events', {from: start_date.to_s, to: (end_date+1).to_s, include: "eventServices", direction: "backward", page: page, limit: 30}) 
        break if response.body["meta"]["count"].to_i == 0
        break if DateTime.parse(response.body["data"][0]["startDate"]) < end_date
        events = response.body["data"]
        ret += events
        page += 1
    end
    return ret   
end

def make_history(events, name, serviceId)
    names = []
    events.each do |event|
        calendar = event["calendar"]["domainIdentifier"].to_i
        next unless calendar == 2 || calendar == 16 
        next unless event["name"].include?(name)
        pname = ""
        eventServices = event["eventServices"]
        eventServices.each do |eventService|
            sId = eventService["serviceId"].to_i
            next unless serviceId == sId
            next if eventService["name"].nil?
            acronym = eventService["name"].split(/ |-/).map { |s| s[0] }.join.upcase 
            if(pname == "")
                pname = acronym
            else
                pname += "&"+acronym
            end
        end
        names << pname
    end
    return names
end

def positions(start_date, end_date)
    tz = TZInfo::Timezone.get('Europe/Berlin')

    # diese positionen sind uns wichtig zu belegen.
    history = history(start_date, start_date - 90)
    important_positions = {1 => "Leitung", 2 => "Predigt"}
    response = $ct.get('api/events', {from: start_date.to_s, to: (end_date+1).to_s, include: "eventServices"}) 
    events = response.body["data"]
    ret = []
    history_cat = {}
    categories = ["deutsch", "russisch", "gemeinsam", "gebet"]
    categories.each do |cat|
        list = {}
        important_positions.each do |pos,name|
            list[pos] = make_history(history, cat, pos)
        end
        history_cat[cat] = list
    end
    events.each do |event|
        # calendar id bekommen-
        calendar = event["calendar"]["domainIdentifier"].to_i
        # wir wollen nur die aus (calendar == 2= oder aus (Kalender == 16 und Gebetsstunde)
        next unless calendar == 2 || (calendar.to_i == 16 && event["name"].include?('Gebet'))
        eventServices = event["eventServices"]
        pos = []
        eventServices.each do |eventService|
            serviceId = eventService["serviceId"].to_i
            history_interncat = nil
            categories.each do |c|
                if event["name"].include?(c)
                    history_interncat = history_cat[c]
                end
            end
            if important_positions.has_key? serviceId
                if eventService["name"].nil?
                    name = "?"
                else
                    name = eventService["name"]
                    if(eventService["agreed"] == false)
                        name += "?"
                    end
                end
                pos << {
                    serviceName: important_positions[serviceId],
                    assignedName: name,
                    history: history_interncat.nil? ? [] : history_interncat[serviceId]
                }
            end

        end
        utc = DateTime.parse(event["startDate"])
        time = tz.to_local(utc)
        ret << {
            name: event["name"],
            date: tz.to_local(utc),
            positions: pos
        }
    end
    return ret
end