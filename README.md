# Church Bulletin

![Logo Banner](./images/cover.png)

A tool to create fast good looking church bulletins. Connects with Nextcloud and ChurchTools.

## Install

```bash
bundle
```

## Use

```bash
ruby create-bulletin.rb
```
