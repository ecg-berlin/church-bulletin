# frozen_string_literal: true
require 'net/dav'
# see https://github.com/devrandom/net_dav
# docs: https://rdoc.info/github/devrandom/net_dav
def get_dav
  dav = Net::DAV.new($config['webdav']['url'] , curl: false)
  dav.verify_server = false
  dav.credentials($config['webdav']['username'], $config['webdav']['password'])
  dav
end

