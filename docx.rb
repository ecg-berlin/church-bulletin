# frozen_string_literal: true

require 'erb'

require 'caracal' # https://github.com/urvin-compliance/caracal

def create_docx(file_name, data)
  Caracal::Document.save './tmp/'+file_name do |docx|
    default_styles(docx)
    # page 1
    docx.p 'Wochenblatt ' + data['date'], style: 'wb_title'
    docx.p
    docx.p data['verse_text'], style: 'vers'
    docx.p data['verse_ref'], style: 'vers'
    docx.p
    docx.h1 "Wochentermine"
    docx.table data['dates'], border_size: 1, border_color: 'ffffff' do
      cell_style cols[0], width: 3000
      cell_style cols[1], width: 1500
      data['important'].each do |c|
        cell_style rows[c], bold: true

      end
      cell_style cells, margins: { top: 0, bottom: 0, left: 0, right: 100 }, style: 'keinleerraum'
    end
  
    # page 2
    docx.page
    docx.p 'Predigtitel', bold: true
    docx.p "Text", style: 'keinleerraum'
    docx.p
    docx.p 'Predigtitel 2', bold: true
    docx.p "Text", style: 'keinleerraum'
    docx.page

    docx.img 'https://ecg.berlin/NL-Notizen.png', width: 595, height: 790 do
      align   :center 
      top     1
      bottom  1
      left    1
      right   1 
    end


    docx.h1 'Ankündigungen'
    data["announcements"].each do |announcement|
      docx.p announcement, style: 'keinleerraum'
    end
    docx.p


    birthdays = Caracal::Core::Models::TableCellModel.new do
      data['birthdays'].each do |b|
        p b, style: 'keinleerraum'
      end
    end

    future = Caracal::Core::Models::TableCellModel.new do
      data['future'].each do |b|
        p b, style: 'keinleerraum'
      end
    end

    docx.table [['Geburtstage', 'Termine zum Vormerken'],[birthdays, future]], border_size: 0 do
      cell_style rows[0], size: 40, top: 0, bold: true, color: '1d1d1d', style: 'keinleerraum'
      cell_style cells, margins: { top: 0, bottom: 0, left: 0, right: 100 }, style: 'keinleerraum'
    end
    docx.p


    kinderstunde = Caracal::Core::Models::TableCellModel.new do
      p 'Kinder sind herzlich willkommen.'
      p 'Wir haben Kinderstunde für Kinder im Alter von 3–9.'
      p 'Für Eltern mit Kindern unter 3 gibt es den Eltern-Kind-Raum als Rückzugsort.', style: 'keinleerraum'
    end

    grundlagenkurs = Caracal::Core::Models::TableCellModel.new do
      p 'Bist du am Glauben interessiert? Möchtest du Mitglied werden? Komme zum Grundlagenkurs, wo du Gottes Wort und uns als Gemeinde kennenlernen kannst!', style: 'keinleerraum'
    end

    docx.table [['Kinderstunde','Grundlagenkurs'], [kinderstunde, grundlagenkurs]], border_size: 0 do
      cell_style rows[0], size: 40, top: 0, bold: true, color: '1d1d1d', style: 'keinleerraum'
      cell_style cells, margins: { top: 0, bottom: 0, left: 0, right: 100 }, style: 'keinleerraum'
    end

    docx.p
    docx.p
    docx.p
    docx.p 'E-Mail: info@ecg.berlin | Webseite: www.ecg.berlin', style: 'klein'
    docx.p 'Pastor Alexander Arzer: 0176/23114017 | Pastor Johann Friesen: 0176/10327819 | Pastor Paul Walger 0176/91327761', style: 'klein'
    docx.p 'Spende:  IBAN DE12 3702 0500 0001 5880 00 | BIC: BFSW DE33 XXX', style: 'klein'
    docx.p 'Spende über Paypal: paypal@ecg.berlin', style: 'klein'

  end

end

def default_styles(docx)
  defaultfont = "Source Serif Pro"

  docx.font do
    name defaultfont
  end

  docx.page_size do
    width       11905.51 # twips
    height      16837.795
    orientation :portrait
  end
  docx.page_margins do
    left    720 # twips
    right   720
    top     720
    bottom  720
  end
  docx.style do
    id              'Normal'
    name            'normal'
    font            defaultfont
    color           '000000'
    size            32 # half-point
    line            280
    align           :left
  end
  docx.style do
    id              'Standard'
    name            'standard'
    font            defaultfont
    color           '000000'
    size            32 # half-point
    line            280
    align           :left
  end
  docx.style do
    id              'default'
    name            'default'
    font            defaultfont
    color           '000000'
    size            32 # half-point
    line            280
    align           :left
  end
  docx.style do
    id              'keinleerraum'
    name            'Kein Leerraum'
    font            defaultfont
    color           '000000'
    size            32 # half-point
    line            280
    align           :left
  end
  docx.style do
    id              'wb_title'
    name            'wb_title'
    font            defaultfont
    color           '000000'
    size            56
    align           :center
  end
  docx.style do
    id              'Heading1'
    name            'heading 1'
    font            defaultfont
    color           '1d1d1d'
    size            40
    bold            true
    align           :left
  end

  docx.style do
    id   'vers'
    name 'Vers'
    size 28
    italic true
    align :center
  end

  docx.style do
    id   'klein'
    name 'klein'
    size 28
    line 250
    align :center
  end
end
