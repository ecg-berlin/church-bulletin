# frozen_string_literal: true

require 'net/dav'
require 'optparse'
require 'fileutils'
require 'pp'
require 'yaml'
require 'date'
require 'time'
require 'r18n-core'
require 'pony'
require 'csv'

require_relative 'docx'
require_relative 'webdav'
require_relative 'church-tools'


def main
  R18n.set('de')
  options = {}
  options[:config] = './secrets.yml'
  options[:env] = false
  options[:dry] = false

  OptionParser.new do |opts|
    opts.banner = 'Usage: create-bulletin.rb [options]'
    opts.on('-c PATH', '--config PATH', 'Path for config yml file') do |v|
      options[:config] = v
      options[:env] = false
    end

    opts.on('-e', '--env', 'Load from env') do |v|
      options[:env] = true
    end

    opts.on('--dry', 'dry run') do |v|
      options[:dry] = true
    end
    
  end.parse!

  if options[:env]
    if  ENV['CHURCHTOOLS_TOKEN'] == nil
      puts "no env CHURCHTOOLS_TOKEN set"
      exit 1
    end
    $config = {
      'webdav' => {
          'url' => ENV['WEBDAV_URL'],
          'password' => ENV['WEBDAV_PASSWORD'],
          'username' => ENV['WEBDAV_USERNAME'],
      },
      'churchtools' => {
          'login_token' => ENV['CHURCHTOOLS_TOKEN'],
          'url' => ENV['CHURCHTOOLS_URL'],
      },
      'mail' => {
        'address' => ENV['MAIL_ADDRESS'],
        'port' => ENV['MAIL_PORT'],
        'user_name' => ENV['MAIL_USER_NAME'],
        'password' => ENV['MAIL_PASSWORD'],
        'from' => ENV['MAIL_FROM'],
        'to' => ENV['MAIL_TO'],
      },
    }
  else
    $config = YAML.load_file(options[:config])
  end

  $config[:dry] = options[:dry]

  if ENV['CREATE_TEST_BULLETIN'] && ENV['CREATE_TEST_BULLETIN'] == "true"
    $config[:create_test_bulleting] = true
  else
    $config[:create_test_bulleting] = false
  end

  ct_init()

  today = Date.today
  sunday1 = today + ((0 - today.wday) % 7)
  sunday2 = sunday1 + 7 # 2015-02-22

  open_pos = positions(today, sunday2)

  events = events(sunday1, sunday1)
  gdg_year = nil
  gdg_lection = nil
  events.each do |event|
      next if event['description'].nil?
      m = event['description'].match /J(\d)#((\w\.\d)|\d+)/
      if m
        gdg_year = m[1]
        gdg_lection = m[2]
        break
      end
  end

  gdg_data = CSV.parse(File.read("./assets/gdg.csv"), headers: true)
  verse_text = 'Barmherzig und gnädig ist der Herr, geduldig und von großer Güte.'
  verse_ref = "(Psalm 103,8)"
  gdg_data.each do |item|
    if item["Jahr"] == gdg_year && item["Lektion"] == gdg_lection
      pp item
      verse_text = item["Vers kurz"]
      verse_ref = "("+item["Versstelle kurz"]+" Lektion #"+gdg_lection+")"
      break
    end
  end


  termine = termine([16], sunday1, sunday2)

  events = events(sunday1, sunday2)

  birthdays = birthdays(sunday1, sunday2)
  birthdays_strings = []
  birthdays.each do |b|
    date = Date.iso8601(b["date"])
    birthdays_strings << date.strftime("%d.%m.") + " " + b["person"]["domainAttributes"]["firstName"] + " " +  b["person"]["domainAttributes"]["lastName"][0] +"."
  end

  dav = get_dav
  dav_root = $config['webdav']['url']
  future = []
  dav.get(dav_root + "/ZumVormerken.txt") do |x|
    future = x.split("\n")
  end
  announcements = []
  dav.get(dav_root + "/Ansagen.txt") do |x|
    announcements = x.split("\n")
  end

  data = {
    'date' => sunday1.strftime("%d.%m.%y"),
    'dates' => [],
    'verse_text' => verse_text,
    'verse_ref' => verse_ref,
    'birthdays' => birthdays_strings,
    'future' => future,
    'announcements' => announcements,
    'important' => [],
  }
  important = []

  counter = 0
  termine.each do |termin|
    name = termin['bezeichnung']
  
    date = Date.parse(termin['startdate'])
    time = Time.parse(termin['startdate'])

    next if date < sunday1
    next if date > sunday2

    next if date == sunday1 && time.hour < 12

    calender = termin['category_id'].to_i

    if date == sunday1 
      datetext = "Diesen Sonntag"
    elsif date == sunday2
      datetext = "Nächsten Sonntag"
    else
      datetext = R18n.l(date, '%A')
    end

    if name.include?('Gebets') || name.include?('Gottesdienst')
      important << counter
    end

    item = [datetext, time.strftime("%H:%M"), name.to_s] # Convert name to string
    data["dates"] << item
    counter = counter + 1
  end

  data['important'] = important

  $config['start_date'] = sunday1
  $config['end_date'] = sunday2

  pp data

  create_docx 'final.docx', data

  pp `ls ./tmp`
  pp `soffice --headless --convert-to odt ./tmp/final.docx`
  pp `ls ./tmp`
  pp `soffice --headless --convert-to docx ./final.odt`
  pp `ls ./tmp`

  path = dav_root + "/" + sunday1.year.to_s
  
  dav.mkdir path unless dav.exists? path
  
  file = File.open("./final.docx", "rb")
  contents = file.read
  dav.put_string(path + "/Wochenblatt-#{sunday1.iso8601}.docx", contents) unless $config[:create_test_bulleting] == true || $config[:dry] == true

  text_preview = "<p>Hi,
  könntest du das vorläufige Wochenblatt (Termine und Ankündigungen) durchschauen von " + Time.now.strftime("%d.%m.%Y %H:%M") + ".<p>
  <p>Falls Termine fehlen oder falsch sind, kann man sie in <a href='https://ecghellersdorf.church.tools/?q=churchcal#CalView/church.tools'> church.tools</a> ändern.</p>
  <p>Falls die Ansagen falsch oder fehlerhaft sind, dann kann man in der Nextcloud unter <a href='https://cloud.ecg.berlin/index.php/f/58752'>Wochenblatt/Ansagen.txt</a> oder unter <a href='https://cloud.ecg.berlin/index.php/f/58752'>Wochenblatt/ZumVormerken.txt</a>ändern.</p>
  <p>Dies sollte bis zum Donnerstag abend passieren. Danach wird das Wochenblatt erstellt. </p>
  <p>Die Gliederungen werden erst nach Freitag hinzugefügt.</p> 
  <p>Falls du das Wochenblatt durchgeschaut hast und alles deiner Meinung nach korrekt war oder du etwas korrigiert hast, dann kannst du gerne auf diese EMail allen antworten.</p>
  "


  text = "<p>Hi,
  das Wochenblatt wurde um " + Time.now.strftime("%d.%m.%Y %H:%M") + " generiert und liegt in der Cloud.<p>
  <p>Falls Termine/Ansagen fehlen oder falsch sind, dann muss man sie in der <a href='https://cloud.ecg.berlin/index.php/apps/files/?dir=/Wochenblatt/"+sunday1.year.to_s+"'>Nextcloud</a> im Dokument direkt ändern</p>
  <p>Die Gliederungen kann nun hinzugefügt werden.</p> 
  "

  text = text_preview if($config[:create_test_bulleting] == true)

  text += "<ul>"
  open_pos.each do |pos|
    text += "<li>"+pos[:name]+"("+ pos[:date].strftime("%d.%m.%Y %H:%M") + ")"
    
    text += "<ul>"

    pos[:positions].each do |people|
      text +=  "<li><b>"+ people[:serviceName] + "</b>: " + people[:assignedName] 
      if(people[:assignedName] == "?")
        text += " [Bisher: " + people[:history].join(",")+"]"
      end
      text += "</li>"
    end

    text += "</ul>"

    text += "</li>"
  end
  text += "</ul>"

  attachments = {"Wochenblatt-#{sunday1.iso8601}.docx" => contents}

  title = "Wochenblatt #{sunday1.iso8601}"
  title = "Vorläufiges Wochenblatt #{sunday1.iso8601}" if($config[:create_test_bulleting] == true)

  Pony.mail({
    to: $config['mail']['to'],
    from: $config['mail']['from'],
    attachments: attachments,
    html_body: text,
    subject: title,
    via: :smtp,
    via_options: {
      address: $config['mail']['address'],
      port: $config['mail']['port'],
      user_name: $config['mail']['user_name'],
      password: $config['mail']['password'],
      authentication: :login,
      ssl: true
    }
  }) unless $config[:dry] == true

end

main
