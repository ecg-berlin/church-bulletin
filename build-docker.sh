#!/bin/bash
set -e
sudo service docker start
sudo docker build --no-cache -t registry.gitlab.com/ecg-berlin/church-bulletin/base-27 .
sudo docker push registry.gitlab.com/ecg-berlin/church-bulletin/base-27

